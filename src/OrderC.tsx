import React, { useState, useEffect, useCallback } from 'react';
import { useFormState } from 'react-use-form-state'
import { RootState } from './store';
import { useSelector, useDispatch } from 'react-redux';
// import { push } from 'connected-react-router';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import Nav from './Nav';
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import Signature from './Signature'
// import TbTicket from './TbTicket';
// import TbPlanSerCate from './TbPlanSer';
import TbCurTicket from './TbCurTicket';
import TbFseSerCate from './TbFseSer';
import { getNextOrderId, updateOrderRow, addOrderRow_ticket, addOrderRow_planSerCate, addOrderRow_fseSerCate } from './orderRows/action';

import TbTicket from './TbTicket';
import { Ticket } from './ticketRows/reducer';
import TbPlanSerCate from './TbPlanSer';
import { PlanSerCate, FseSerCate } from './orderRows/reducer';
import { push } from 'connected-react-router';
import { delPlanSerCateTb } from './planSerCateRows/action';
import { delTicketTb } from './ticketRows/action';
import { delFseSerCateTb } from './fseSerCateRows/action';
// import { Ticket } from './ticketRows/reducer';
function Order() {
  const [formState, { text }] = useFormState();
  const [showSignatureBox, setShowSignatureBox] = useState(false)
  const [sideDate_unix, setSideDate_unix] = useState(0)
  const [cusType, serCusType] = useState("")
  const [selectedSiteDate, setSelectedSiteDate] = useState<Date | [Date, Date] | null>(null)


  const dispatch = useDispatch()
  // const orderStatusArr = [{name:""},{name:"Open"},{name:"Assigned"},{name:"Execute"}]
  const new_fseSerCate_rows = useSelector((state: RootState) => state.fseSerCate.fseSerCate) as FseSerCate[]
  const new_planSerCate_rows = useSelector((state: RootState) => state.planSerCate.planSerCate) as PlanSerCate[]
  const new_ticket_rows = useSelector((state: RootState) => state.tickets.tickets) as Ticket[]
  
  const order_detail = useSelector((state: RootState) => state.order.order)
  // console.log(order_rows, "orderRow")
  
  
  const load = useCallback(async function () {
    // const res = await fetch(`${process.env.REACT_APP_BACKEND_URL}/todos?projectId=` + projectId);
    // const json = await res.json();
    dispatch(getNextOrderId())
    dispatch(delPlanSerCateTb())
    dispatch(delTicketTb())
    dispatch(delFseSerCateTb())
    
  }, [dispatch]);

  useEffect(() => {
    load();
    
  }, [load])
  const curOid = useSelector((state: RootState) => {
    let oidArr = []
    for (const oid of state.order.order) {
      oidArr.push(oid.oid)
    }
    const nextOID: number = Math.max(...oidArr)
    console.log(nextOID,"next")
    return nextOID
  })

  let submitObj = {}
  function save() {
    submitObj = { mode: "draft" }
    if (cusType !== "") {
      Object.assign(submitObj, { cusType: cusType })
    }
    if (formState.values.cusId !== "") {
      Object.assign(submitObj, { cusId: formState.values.cusId })
    }
    if (formState.values.orderStatus !== "") {
      Object.assign(submitObj, { orderStatus: formState.values.orderStatus })
    }
    if (sideDate_unix > 0) {
      Object.assign(submitObj, { siteDate: sideDate_unix })
    }
    // console.log(submitObj)
    dispatch(updateOrderRow(submitObj, curOid))
    dispatch(addOrderRow_ticket(new_ticket_rows,curOid))
    dispatch(addOrderRow_planSerCate(new_planSerCate_rows,curOid))
    dispatch(addOrderRow_fseSerCate(new_fseSerCate_rows,curOid))
    console.log(order_detail,"for submit to BN")
    dispatch(delPlanSerCateTb())
    dispatch(delTicketTb())
    dispatch(delFseSerCateTb())
    dispatch(push('/searchOrder'))
  }
  function submit() {
    submitObj = { mode: "confirm" }
    if (cusType !== "") {
      Object.assign(submitObj, { cusType: cusType })
    }
    if (formState.values.cusId !== "") {
      Object.assign(submitObj, { cusId: formState.values.cusId })
    }
    if (formState.values.orderStatus !== "") {
      Object.assign(submitObj, { orderStatus: formState.values.orderStatus })
    }
    if (sideDate_unix > 0) {
      Object.assign(submitObj, { siteDate: sideDate_unix })
    }
    dispatch(updateOrderRow(submitObj, curOid))
    dispatch(addOrderRow_ticket(new_ticket_rows,curOid))
    dispatch(addOrderRow_planSerCate(new_planSerCate_rows,curOid))
    dispatch(addOrderRow_fseSerCate(new_fseSerCate_rows,curOid))
    console.log(order_detail,"for submit to BN")
    dispatch(delPlanSerCateTb())
    dispatch(delTicketTb())
    dispatch(delFseSerCateTb())
    dispatch(push('/searchOrder'))
  }
  function handleSelDate(date: Date | [Date, Date] | null, event: React.SyntheticEvent<any, Event> | undefined) {
    const myDate = date as Date
    moment(myDate).format("DD-MMM-YYYY")
    setSideDate_unix(new Date(myDate).getTime())
    setSelectedSiteDate(myDate)

  }
  return (
    <>
      <Nav />
      <div className="br"></div>
      <div className="container-fluid">

        <form className="orderForm"

        >



          <div className="box">
            <div className="row">
              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>Customer Type</label><br></br>
                <label>HKBN</label>
                <input className="radioBtn" type="radio" name="cusType" value="HKBN" onClick={event => serCusType(event.currentTarget.value)} />
                <label>WTNT</label>
                <input className="radioBtn" type="radio" name="cusType" value="WTNT" onClick={event => serCusType(event.currentTarget.value)} />

              </div>

              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>Customer ID<br></br>
                  <input {...text('cusId')} />
                </label>
              </div>

              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>Order Status<br></br>
                  <input {...text('orderStatus')} value="Open" disabled />
                </label>
              </div>




              <div className="col-lg-3 col-md-6 col-sm-12">
                <label>On Site Date<br></br>
                  <DatePicker
                    onChange={handleSelDate}
                    selected={selectedSiteDate as Date}
                    dateFormat={'dd-MM-yyyy'}
                    minDate={new Date()}
                  />
                </label>
              </div>

            </div>
          </div>
          </form>

          <div className="box">

            <div className="row">

              <div className="col-lg-3 col-md-6 col-sm-12">
                <TbCurTicket />
              </div>

              <div className="col-lg-3 col-md-6 col-sm-12">
                <TbTicket />            
              </div>

              <div className="col-lg-3 col-md-6 col-sm-12">
                <TbPlanSerCate />       
              </div>

              <div className="col-lg-3 col-md-6 col-sm-12">
                <TbFseSerCate />
              </div>

            </div>

            <div className="row">
              <div className="col-lg-5">
                <div className="signature-box" data-show={showSignatureBox}>
                  <Signature />
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <div className="flex-div-end">
                <button type="button" onClick={save} className="btn btn-info btn-md" >Save</button>
                <button type="button" onClick={submit} className="btn btn-info btn-md" >Submit</button>
                {!showSignatureBox && <button onClick={() => setShowSignatureBox(!showSignatureBox)} type="button" className="btn btn-info btn-md">Signature</button>}
                {showSignatureBox && <button disabled className="btn btn-info btn-md">Signature</button>}
              </div>
            </div>
          </div>







      </div>


    </>
  )
}
export default Order




