import { FseSerCateActions } from "./action";
// reducer is to store old and new status of memo
export interface FseSerCate {
  fseSerCate: string;
  qty: number;
}

export interface FseSerCateState {
  fseSerCate: FseSerCate[]
}

const initialState: FseSerCateState = {
  fseSerCate: [

  ]
};

// because action.ts create action name, we can switch relative method depends on action name
export const FseSerCateReducer = (oldState: FseSerCateState = initialState, action: FseSerCateActions): FseSerCateState => {
  switch (action.type) {
    case "@@FSE_SER_CATE/LOADED_ROW":
      // return the ...oldState and memos: action.memos new state 
      return {
        ...oldState, 
        fseSerCate: action.fseSerCate_rows
      }
    case "@@FSE_SER_CATE/ADD_ROW":
      const oldArr = oldState.fseSerCate.slice();
      const newArr = action.arr as FseSerCate[] 
      const newRows = oldArr.concat(newArr)
      // console.log(newRows)
      // newMemos[action.i].content = action.content
      return {
        ...oldState,
        fseSerCate: newRows
      }
    case "@@FSE_SER_CATE/DEL_ROW":
      const oldArr2 = oldState.fseSerCate.slice();
      oldArr2.splice(action.idx,1)
      // console.log(oldArr2)
      return {
        ...oldState,
        fseSerCate: oldArr2
      }
      case "@@FSE_SER_CATE/DEL_TB":
      const oldArr1 = oldState.fseSerCate.slice();
      oldArr1.splice(0)
      // console.log(oldArr2)
      return {
        ...oldState,
        fseSerCate: oldArr1
      }
  }
  
  return oldState;
}
