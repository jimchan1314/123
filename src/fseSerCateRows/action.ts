import { FseSerCate } from "./reducer";
// action is to create action name for reducer to call the relative method
export function loadFseSerCateRow(fseSerCate_rows: FseSerCate[]) {
  return {
    type: "@@FSE_SER_CATE/LOADED_ROW" as  "@@FSE_SER_CATE/LOADED_ROW",
    fseSerCate_rows
  };
}

export function addFseSerCateRow(obj: Object) {
  let arr = []
  arr.push(obj)
  // console.log(arr)
  // console.log(obj)
  return {
    type: "@@FSE_SER_CATE/ADD_ROW" as  "@@FSE_SER_CATE/ADD_ROW",
    arr
  };
}
export function delFseSerCateRow(idx: number) {
  
  // console.log(idx)
  // console.log(obj)
  return {
    type: "@@FSE_SER_CATE/DEL_ROW" as  "@@FSE_SER_CATE/DEL_ROW",
    idx
  };
}
export function delFseSerCateTb() {
  
  // console.log(idx)
  // console.log(obj)
  return {
    type: "@@FSE_SER_CATE/DEL_TB" as  "@@FSE_SER_CATE/DEL_TB",
    
  };
}

export type FseSerCateActions = ReturnType<typeof loadFseSerCateRow> | ReturnType<typeof addFseSerCateRow> | 
ReturnType<typeof delFseSerCateRow> | ReturnType<typeof delFseSerCateTb>
