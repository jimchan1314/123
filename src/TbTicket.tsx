import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
// import { RootState } from "./store"
import { addTicketRow, delTicketRow } from "./ticketRows/action"
// import { push } from "connected-react-router"
import { RootState } from "./store"

function TbTicket() {
  
    const [purTicketModal, setPurTicketModal] = useState(false)
    const [planCode, setPlanCode] = useState("")
    const PlanArr = [{name:""},{name:"PlanA"},{name:"PlanB"},{name:"PlanC"}]
    const [qty, setQty] = useState(0)
    const [ticketCount, setTicketCount] = useState(0)
    const dispatch = useDispatch()
    const new_ticket_rows = useSelector((state: RootState) => state.tickets)
    
    // console.log(new_ticket_rows,"ssssssssssssssssssssssss")  
    const purTicketTbObj = {}
    // const purTicketTbRows = [{planCode:"A",qty:2,ticketCount:3},{planCode:"B",qty:2,ticketCount:3},{planCode:"C",qty:2,ticketCount:3}]
    function save(){
        Object.assign(purTicketTbObj,{planCode:planCode})
        Object.assign(purTicketTbObj,{qty:qty})
        Object.assign(purTicketTbObj,{ticketCount:ticketCount})
        // console.log(purTicketTbObj)
        
        setPurTicketModal(!purTicketModal)
        dispatch(addTicketRow(purTicketTbObj))
    }
    
    return (
    <>
  <div className="Ma">
                  <table className="purTicketTb">
                    <tbody>
                      <tr>
                        <th className="col_1">
                          Plan Code
              </th>
                        <th className="col_2">
                          Qty
              </th>
                        <th className="col_3">
                          Ticket Count
              </th>
                        <th className="col_4">
                          <button className="icon_btn" onClick={() => setPurTicketModal(!purTicketModal)}><i className="fas fa-search"></i></button>
                        </th>
                      </tr>

                      {new_ticket_rows.tickets.length > 0 && new_ticket_rows.tickets.map((obj, idx) => {
                        let lists = []
                        lists.push(
                          <tr key={idx}>
                            <td className="col_1">
                              {obj.planCode}
                            </td>
                            <td className="col_2">
                              {obj.qty}
                            </td>
                            <td className="col_3">
                              {obj.ticketCount}
                            </td>
                            <td className="col_4">
                              <button className="icon_btn" onClick={event => dispatch(delTicketRow(idx))}><i className="fas fa-times"></i></button>
                            </td>
                          </tr>
                        )
                        return lists
                      })}


                    </tbody>
                  </table>
                </div>    
    <div className="purTicketModal" data-show={purTicketModal}>
       <div>
            <label>Plan Code<br></br>
            <select className="selection" value={planCode} onChange={event=>{setPlanCode(event.currentTarget.value);setTicketCount(2);}}>
            {PlanArr.map((obj,idx) => <option key={idx} value={obj.name}>{obj.name}</option>)}          
            </select>
            </label>
       </div>
       <div>
            <label>Qty<br></br>
            <input type="number" onBlur={event=>{setQty(parseInt(event.target.value))}}/>
            </label>
       </div>
       <div>
            <label>Ticket Count<br></br>
            <input type="text" value={ticketCount} disabled/>
            </label>       
        </div>
        <div>
        <button onClick={save} type="button" className="btn btn-info btn-md">Save</button>
        <button onClick={event=>{setPurTicketModal(!purTicketModal)}} type="button" className="btn btn-info btn-md">Cancel</button>      
        </div>    
    </div>

    
    </>
    )
}
export default TbTicket
