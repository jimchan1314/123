// import { Order } from "./reducer";
import { Ticket } from "../ticketRows/reducer";
import { PlanSerCate } from "../planSerCateRows/reducer";
import { FseSerCate } from "../fseSerCateRows/reducer";
// import { Ticket } from "../ticketRows/reducer";
// action is to create action name for reducer to call the relative method

export function getNextOrderId() {
  return {
    type: "@@ORDER/GET_NEXT_OID" as "@@ORDER/GET_NEXT_OID",
  };
}

export function updateOrderRow(obj: Object,curOid:number) {
  // let arr = []
  // arr.push(obj)
  // console.log(arr)
  // console.log(obj)
  return {
    type: "@@ORDER/UPDATE_ROW" as "@@ORDER/UPDATE_ROW",
    obj,
    curOid,
  };
}

export function loadOrderRow(oid: number) {
  
  return {
    type: "@@ORDER/LOADED_ROW" as "@@ORDER/LOADED_ROW",
    oid
  };
}

export function addOrderRow_ticket(arr: Ticket[],curOid2:number) {  
  return {
    type: "@@ORDER/ADD_ROW_TICKET" as "@@ORDER/ADD_ROW_TICKET",
    arr,
    curOid2,
  };
}
export function addOrderRow_planSerCate(arr: PlanSerCate[],curOid2:number) {  
  return {
    type: "@@ORDER/ADD_ROW_PLAN-SER-CATE" as "@@ORDER/ADD_ROW_PLAN-SER-CATE",
    arr,
    curOid2,
  };
}
export function addOrderRow_fseSerCate(arr: FseSerCate[],curOid2:number) {  
  return {
    type: "@@ORDER/ADD_ROW_FSE-SER-CATE" as "@@ORDER/ADD_ROW_FSE-SER-CATE",
    arr,
    curOid2,
  };
}
export function delOrderRow(idx: number) {
  
  // console.log(idx)
  // console.log(obj)
  return {
    type: "@@ORDER/DEL_ROW" as  "@@ORDER/DEL_ROW",
    idx
  };
}

export type OrderActions = ReturnType<typeof getNextOrderId> |
 ReturnType<typeof updateOrderRow> | ReturnType<typeof loadOrderRow> |
  ReturnType<typeof addOrderRow_ticket> | ReturnType<typeof delOrderRow> |
  ReturnType<typeof addOrderRow_planSerCate> | ReturnType<typeof addOrderRow_fseSerCate>
