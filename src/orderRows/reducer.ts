import { OrderActions } from "./action";

// reducer is to store old and new status of memo
export interface PlanSerCate {
  planSerCate:string;
  qty:number;
}
export interface FseSerCate {
  fseSerCate:string;
  qty:number;
}
export interface PurTicket {
  planCode:string;
  qty:number;
  ticketCount:number;
}
export interface CurTicket {
  planCode:string;
  qty:number;
  ticketCount:number;
}

export interface Order {
  oid:number;
  mode: string;
  cusType: string;
  cusId:string;
  orderStatus:string;
  siteDate:number;
  planSerCate:PlanSerCate[]
  fseSerCate:FseSerCate[]
  purTicket:PurTicket[]
  curTicket:CurTicket[]

}

export interface OrderState {
  order: Order[]
}

const initialState: OrderState = {
  order: [
    
  ]
};

// because action.ts create action name, we can switch relative method depends on action name
export const OrderReducer = (oldState: OrderState = initialState, action: OrderActions): OrderState => {
  switch (action.type) {
    case "@@ORDER/GET_NEXT_OID":
        let nextOID
        let oldArr = oldState.order.slice();
        if(oldArr.length === 0){
          nextOID = 10001
        }else if(oldArr.length > 0){
            let oidArr = []
            for(const oid of oldArr){
              oidArr.push(oid.oid)
            }
          nextOID = Math.max(...oidArr) + 1
          }
          const newArr = [
            {
            oid:nextOID,
            mode: "",
            cusType: "",
            cusId:"",
            orderStatus:"",
            siteDate:0,
            planSerCate:[
              {
              planSerCate:"",
              qty:0,
              }
            ],
            fseSerCate:[
              {
                fseSerCate:"",
                qty:0,
              }
            ],
            purTicket:[
              {
              planCode:"",
              qty:0,
              ticketCount:0,
              }
            ],
            curTicket:[
              {
                planCode:"",
                qty:0,
                ticketCount:0,
              }
            ]
            }
          ] as Order[]
          // oldArr = newArr
          const newRows = oldArr.concat(newArr)
          // console.log(newRows)
          return {
            ...oldState,
            order: newRows
          }
        
      
        

    case "@@ORDER/UPDATE_ROW":
    
    const oldArr3 = oldState.order.slice();

      for(const obj of oldArr3){
        if(obj.oid === action.curOid){
          Object.assign(obj, action.obj);
          
          // console.log(oldArr3,"new")
          return {
            ...oldState,
            order: oldArr3
          }
        }
      }
      return oldState      
    case "@@ORDER/LOADED_ROW":
      const oldArr5 = oldState.order.slice();
      for(const obj of oldArr5){
        if(obj.oid === action.oid){
          const newArr = [obj]
          // console.log(oldArr1,"final")
          return {
            ...oldState,
            order: newArr
          }    
        }
      }
      return oldState      
      case "@@ORDER/ADD_ROW_TICKET":
      const oldArr1 = oldState.order.slice();
      for(const obj of oldArr1){
        if(obj.oid === action.curOid2){
          obj.purTicket = action.arr
          console.log(oldArr1,"final")
          return {
            ...oldState,
            order: oldArr1
          }    
        }
      }
      return oldState      
      

      // newMemos[action.i].content = action.content
      case "@@ORDER/ADD_ROW_PLAN-SER-CATE":
      const oldArr2 = oldState.order.slice();
      for(const obj of oldArr2){
        if(obj.oid === action.curOid2){
          obj.planSerCate = action.arr
          console.log(oldArr2,"final")
          return {
            ...oldState,
            order: oldArr2
          }    
        }
      }
      return oldState

      case "@@ORDER/ADD_ROW_FSE-SER-CATE":
      const oldArr4 = oldState.order.slice();
      for(const obj of oldArr4){
        if(obj.oid === action.curOid2){
          obj.fseSerCate = action.arr
          console.log(oldArr4,"final")
          return {
            ...oldState,
            order: oldArr4
          }    
        }
      }
      return oldState
  }
  
  return oldState;
}
