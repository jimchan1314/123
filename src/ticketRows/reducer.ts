import { TicketsActions } from "./action";
// reducer is to store old and new status of memo
export interface Ticket {
  planCode: string;
  qty: number;
  ticketCount:number;
}

export interface TicketState {
  tickets: Ticket[]
}

const initialState: TicketState = {
  tickets: [
    
  ]
};

// because action.ts create action name, we can switch relative method depends on action name
export const TicketReducer = (oldState: TicketState = initialState, action: TicketsActions): TicketState => {
  switch (action.type) {
    case "@@TICKETS/LOADED_ROW":
      
      // const oldArr3 = oldState.tickets.slice();
      // console.log(oldArr3,"load ticket")
      
      return {
        ...oldState,
        tickets: action.ticket
      }
    case "@@TICKETS/ADD_ROW":
      const oldArr = oldState.tickets.slice();
      const newArr = action.arr as Ticket[] 
      const newRows = oldArr.concat(newArr)
      // console.log(newRows)
      // newMemos[action.i].content = action.content
      
      return {
        ...oldState,
        tickets: newRows
      }
    case "@@TICKETS/DEL_ROW":
      const oldArr2 = oldState.tickets.slice();
      oldArr2.splice(action.idx,1)
      // console.log(oldArr2,"del")
      
      return {
        ...oldState,
        tickets: oldArr2
      }
      case "@@TICKETS/DEL_TB":
      const oldArr3 = oldState.tickets.slice();
      oldArr3.splice(0)
      // console.log(oldArr3,"del")
      
      return {
        ...oldState,
        tickets: oldArr3
      }
  }
  
  return oldState;
}
