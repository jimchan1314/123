import { Ticket } from "./reducer";
// action is to create action name for reducer to call the relative method
export function loadTicketRow(ticket:Ticket[]) {
  return {
    type: "@@TICKETS/LOADED_ROW" as  "@@TICKETS/LOADED_ROW",
    ticket
  };
}

// export function loadTicketRow(ticket_rows: Ticket[]) {
//   return {
//     type: "@@TICKETS/LOADED_ROW" as  "@@TICKETS/LOADED_ROW",
//     ticket_rows
//   };
// }

export function addTicketRow(obj: Object) {
  let arr = []
  arr.push(obj)
  // console.log(arr)
  // console.log(obj)
  return {
    type: "@@TICKETS/ADD_ROW" as  "@@TICKETS/ADD_ROW",
    arr
  };
}
export function delTicketRow(idx: number) {
  
  // console.log(idx)
  // console.log(obj)
  return {
    type: "@@TICKETS/DEL_ROW" as  "@@TICKETS/DEL_ROW",
    idx
  };
}
export function delTicketTb() {
  
  // console.log(idx)
  // console.log(obj)
  return {
    type: "@@TICKETS/DEL_TB" as  "@@TICKETS/DEL_TB",
    
  };
}
export type TicketsActions = ReturnType<typeof loadTicketRow> | ReturnType<typeof addTicketRow> |
 ReturnType<typeof delTicketRow> | ReturnType<typeof delTicketTb>
