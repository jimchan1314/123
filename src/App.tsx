import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import { Route, Switch } from 'react-router-dom';
import Nav from './Nav';
import SearchOrder from './SearchOrder';
// import Ticket from './Signature'
import Order from './OrderC';

function App() {

  
  return (
    <div className="App">
    
       
      
      

      <Switch>
        <Route path="/" exact><Nav /></Route>


        <Route path="/searchOrder" exact><SearchOrder /></Route>
        <Route path="/order" exact><Order /></Route>
        <Route path="/order/:oid" exact>BACK-end response</Route>
        
        
        {/* <Route path="/ticket" exact><Ticket /></Route> */}
        <Route>404 找不到喎</Route>
      </Switch>
     
     
      {/* <Link to="/scale/:id"></Link> */}
   
    </div>
  );
}

export default App;
