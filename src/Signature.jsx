import React, { useState,useEffect,useRef } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';


function Signature() {
    const canvasRef = useRef(null)
    const contextRef = useRef(null)
    const [show, setShow] = useState(true)
    
    const [showSignPNG,setShowSignPNG] = useState(false)
    const [isDrawing, setIsDrawing] = useState(false)
    const myCanvas = document.querySelector("#myCanvas")
    const imgConverted = document.querySelector("#imgConverted")
    useEffect(()=>{
        const canvas = canvasRef.current;
        canvas.width = 550;
        canvas.height = 300;
        canvas.style.width = `${550}px`;
        canvas.style.height = `${300}px`;
        
        const context = canvas.getContext("2d")
        // context.scale(2,2)
        context.lineCap = "round"
        context.strokeStyle = "black"
        context.lineWidth = 3
        contextRef.current = context;
        
    },[])

    const startDrawing = ({nativeEvent}) =>{
        const {offsetX,offsetY} = nativeEvent;
        contextRef.current.beginPath()
        contextRef.current.moveTo(offsetX,offsetY)
        setIsDrawing(true)
    }

    const finishDrawing = () => {
        contextRef.current.closePath()
        setIsDrawing(false)
    }

    const draw = ({nativeEvent}) => {
        if(!isDrawing){
            return
        }
        const {offsetX,offsetY} = nativeEvent
        contextRef.current.lineTo(offsetX,offsetY)
        contextRef.current.stroke()
    }
    
    function save(){
        const dataURI = myCanvas.toDataURL()
        imgConverted.src = dataURI
        setShow(!show)
        setShowSignPNG(!showSignPNG)
        
    }
    
    function saveToPNG(){
        // alert(window.navigator.msSaveBlob)
        // if(window.navigator.msSaveBlob){
            // window.navigator.msSaveBlob(myCanvas.msToBlob(),"canvas-img.png")
        // }
        const dataURI = myCanvas.toDataURL()
        const a = document.createElement("a")
        a.href = dataURI
        a.download = "canvas-img.png"
        a.click()
    }
    
    return (
        <>
        {show===true && <div className="sign_container">
        <div className="row">
        <canvas id="myCanvas"
        onMouseDown={startDrawing}
        onMouseUp={finishDrawing}
        onMouseMove={draw}
        ref={canvasRef}
        />
        </div>
        </div>}

        <div className="sign_PNG" data-show={showSignPNG}>
        <div className="sign_container">
        <div className="row">
        <img id="imgConverted" alt="imgConverted"></img>
        </div>
        </div>
        </div>
        
        
        
        <div className="flex-div-center">
        <div className="row">
        <button onClick={save} type="button" className="btn btn-info btn-md">Save</button>
        <button onClick={saveToPNG} type="button" className="btn btn-info btn-md">Export to PNG</button>
        
        </div>
        </div>

        
        
        </>
    )
}
export default Signature




  