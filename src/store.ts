import { createStore, combineReducers , compose, applyMiddleware, AnyAction} from 'redux'
import { RouterState, connectRouter, routerMiddleware } from 'connected-react-router';
import { createBrowserHistory } from 'history';
import thunk, { ThunkDispatch as OldThunkDispatch } from 'redux-thunk';
import { TicketReducer, TicketState } from './ticketRows/reducer';
import { PlanSerCateReducer, PlanSerCateState } from './planSerCateRows/reducer';
import { FseSerCateReducer, FseSerCateState } from './fseSerCateRows/reducer';
import { OrderReducer, OrderState } from './orderRows/reducer';
declare global {
  /* tslint:disable:interface-name */
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__: any
  }
}
export type RootAction = AnyAction
export type ThunkDispatch = OldThunkDispatch<RootState, null, RootAction>
export const history = createBrowserHistory();


  export interface RootState {
    order: OrderState,
    tickets: TicketState,
    planSerCate: PlanSerCateState,
    fseSerCate: FseSerCateState,
    router: RouterState,
    
  }
  
  
  // step 2 combine reducer and send to reducer file
  const reducer = combineReducers<RootState>({
    order: OrderReducer,
    tickets: TicketReducer,
    planSerCate:PlanSerCateReducer,
    fseSerCate:FseSerCateReducer,
    router: connectRouter(history)
  })
  
  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  // step 1 create store
  export const store = createStore(reducer,
    composeEnhancers(
        
        applyMiddleware(thunk),
        applyMiddleware(routerMiddleware(history))
  ));
  