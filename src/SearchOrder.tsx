import React, { useState } from 'react';
import { useFormState } from 'react-use-form-state'
// import { RootState } from './store';
// import { useDispatch } from 'react-redux';
// import { push } from 'connected-react-router';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import Nav from './Nav';
import DatePicker from 'react-datepicker'
import "react-datepicker/dist/react-datepicker.css";
import { CSVLink } from 'react-csv';
import { RootState } from './store';
import { useSelector, useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
// import moment from 'moment';
function SearchOrder() {
    const [formState, { text }] = useFormState();
    const [showSearchBox, setShowSearchBox] = useState(false)
    const [orderStatus] = useState()
    const [selectedSiteDateFm, setSelectedSiteDateFm] = useState<Date | [Date,Date] | null>(null)
    const [selectedSiteDateTo, setSelectedSiteDateTo] = useState<Date | [Date,Date] | null>(null)
    const order_TB = useSelector((state: RootState) => state.order.order)
    // console.log(order_TB)
    const dispatch = useDispatch()  
    const orderStatusArr = [{name:""},{name:"Open"},{name:"Assigned"},{name:"Execute"}]
    const csvData = [
        ["firstname", "lastname", "email"],
        ["Ahmed", "Tomi", "ah@smthing.co.com"],
        ["Raed", "Labes", "rl@smthing.co.com"],
        ["Yezzi", "Min l3b", "ymin@cocococo.com"]
      ]
    return (
        <>
        <Nav />
        <div className="container-fluid">
        <div className="row">
            <div className="flex-div-start">
            <h3>Search</h3>
            <button className="icon_btn" onClick={event=>setShowSearchBox(!showSearchBox)}><i className="fas fa-sort-down icon"></i></button> 
            </div>
        </div>

        <form className="searchForm" data-show={showSearchBox} 
        onSubmit={event => {
          event.preventDefault();
          if(formState.values.cusId!==""){
            console.log(formState.values.cusId)
          }
          if(formState.values.oid!==""){
            console.log(formState.values.oid)
          }
          if(formState.values.serCate!==""){
              console.log(formState.values.serCate)
          }
          if(formState.values.fseSerCate!==""){
              console.log(formState.values.fseSerCate)
          }
        }}
        onReset={event => {
            event.preventDefault();
            formState.reset()
        }}
        >
        <div className="box">
        <div className="row">
        <div className="col-lg-3 col-md-6 col-sm-12">
            <label>Customer Type</label><br></br>
            <label>HKBN</label>
            <input className="radioBtn" type="radio" name="cusType" value="HKBN" onClick={event=>alert(event.currentTarget.value)}/>
            <label>WTNT</label>
            <input className="radioBtn" type="radio" name="cusType" value="WTNT" onClick={event=>alert(event.currentTarget.value)}/>
            
        </div>
        <div className="col-lg-3 col-md-6 col-sm-12">
            <label>Customer ID<br></br>
            <input {...text('cusId')} />
            </label>
        </div>
        <div className="col-lg-3 col-md-6 col-sm-12">
            <label>OID<br></br>
            <input {...text('oid')} />
            </label>
        </div>
        </div>
        
        <div className="row">
        <div className="col-lg-3 col-md-6 col-sm-12">
            <label>Order Status<br></br>
            <select className="selection" value={orderStatus} onChange={event=>alert(event.currentTarget.value)}>
            {orderStatusArr.map((obj,idx) => <option key={idx} value={obj.name}>{obj.name}</option>)}          
            </select>
            </label>
        </div>
        <div className="col-lg-3 col-md-6 col-sm-12">
            <label>Service Cate<br></br>
            <input {...text('serCate')} />
            </label>
        </div>
        <div className="col-lg-3 col-md-6 col-sm-12">
            <label>FSE Service Cate<br></br>
            <input {...text('fseSerCate')} />
            </label>
        </div>        
        </div>
 
        <div className="row">
        
        <div className="col-lg-3 col-md-6 col-sm-12">
        <label>On Site Date From<br></br>
        <DatePicker
        onChange={date=>{date && setSelectedSiteDateFm(date);setSelectedSiteDateTo(null)}}
        selected={selectedSiteDateFm as Date}
        dateFormat={'dd-MM-yyyy'}
        minDate={new Date()}
        />
        </label>
        </div>        
        

        
        <div className="col-lg-3 col-md-6 col-sm-12">
        <label>On Site Date To<br></br>
        <DatePicker
        onChange={date=>{date && setSelectedSiteDateTo(date)}}
        selected={selectedSiteDateTo as Date}
        dateFormat={'dd-MM-yyyy'}
        minDate={selectedSiteDateFm as Date}
        />
        </label>
        </div>        
        
        </div>


        <div className="row">
        <div className="col-lg-12">
        <div className="flex-div-end">
        {/* <input type="reset" className="btn btn-info btn-md" value="Reset" /> */}
        <input type="submit" className="btn btn-info btn-md" value="Search" />
                <CSVLink className="btn btn-info btn-md" data={csvData}>
                Export CSV
                </CSVLink>
                
        </div>
        </div>
        </div>
        </div>
        </form>
        
        <div className="row">
        <div className="col-lg-12">
        <div className="Ma">
            <table className="OrderReportTb">
            <tbody>
              <tr>
              <th className="col_1">
              Customer Type
              </th>
              <th className="col_2">
              Customer ID
              </th>
              <th className="col_3">
              OID
              </th>
              <th className="col_4">
              Order Status
              </th>
              <th className="col_5">
              Service Cate
              </th>
              <th className="col_6">
              FSE Service Cate
              </th>
              <th className="col_7">
              On Site Date
              </th>
              <th className="col_8">
              
              </th>

              </tr>
              
              
              {order_TB.length>0 && order_TB.map((obj,idx) => {
                let lists = []
                const planSerCateArr = obj.planSerCate.map((obj) =>obj.planSerCate)
                const planSerStr = planSerCateArr.join()
                const fseSerCateArr = obj.fseSerCate.map((obj) =>obj.fseSerCate)
                const fseSerStr = fseSerCateArr.join()
                // const purTicketArr = obj.purTicket.map((obj) =>obj.planCode)
                // const purTicketStr = purTicketArr.join()
                // console.log(planSerStr)
                // const curPlan = obj.planSerCate[idx]
                // console.log(curPlan.planSerCate) 
                // console.log(obj.planSerCate[idx].planSerCate)
                lists.push(
                <tr key={idx}>
                <td className="col_1">
                {obj.cusType}
                </td>
                <td className="col_2">
                {obj.cusId}
                </td>
                <td className="col_3">
                {obj.oid}
                </td>
                <td className="col_4">
                {obj.orderStatus}
                </td>
                <td className="col_5">
                {planSerStr}
                </td>
                <td className="col_6">
                {fseSerStr}
                </td>
                <td className="col_7">
                {obj.siteDate}
                </td>
                <td className="col_8">
                <button className="icon_btn" onClick={event=>dispatch(push(`/Order/${obj.oid}`))}><i className="far fa-edit"></i></button>
                </td>
                
                </tr>
                )
                return lists
              })}
              
              
            </tbody>
            </table>
    </div>

    </div>
    </div>
        </div>
        

        </>
    )
}
export default SearchOrder




  