import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { addPlanSerCateRow, delPlanSerCateRow } from "./planSerCateRows/action"
import { RootState } from "./store"

function TbPlanSerCate() {
    const [planSerCateModal, setPlanSerCateModal] = useState(false)    
    const [planSerCate, setPlanSerCate] = useState("")
    const SerArr = [{name:""},{name:"SerPlanA"},{name:"SerPlanB"},{name:"SerPlanC"}]
    const [qty, setQty] = useState(0)
    const dispatch = useDispatch()
    const new_planSerCate_rows = useSelector((state: RootState) => state.planSerCate.planSerCate)  
    
    // console.log(ticket_rows)  
    const planSerCateTbObj = {}
    // const planSerCateTbRows = [{planSerCate:"A",qty:2},{planSerCate:"B",qty:3},{planSerCate:"C",qty:3}]
    function save(){
        Object.assign(planSerCateTbObj,{planSerCate:planSerCate})
        Object.assign(planSerCateTbObj,{qty:qty})
        
        // console.log(purTicketTbObj)
        dispatch(addPlanSerCateRow(planSerCateTbObj))
        setPlanSerCateModal(!planSerCateModal)
    }
    return (
    <>
  
    <div className="Ma">
            <table className="planSerTb">
            <tbody>
              <tr>
              <th className="col_1">
                Service Cate
              </th>
              <th className="col_2">
                Qty
              </th>
              <th className="col_3">
              <button className="icon_btn" onClick={() => setPlanSerCateModal(!planSerCateModal)}><i className="fas fa-search"></i></button>
              </th>
              </tr>
              
              
              {new_planSerCate_rows.length>0 && new_planSerCate_rows.map((obj,idx) => {
                let lists = []
                lists.push(
                <tr key={idx}>
                <td className="col_1">
                {obj.planSerCate}
                </td>
                <td className="col_2">
                {obj.qty}
                </td>
                <td className="col_3">
                <button className="icon_btn" onClick={event=>dispatch(delPlanSerCateRow(idx))}><i className="fas fa-times"></i></button>
                </td>
                
                </tr>
                )
                return lists
              })}
              
              
            </tbody>
            </table>
    </div>        


    <div className="planSerCateModal" data-show={planSerCateModal}>
       <div>
            <label>Plan Service Category<br></br>
            <select className="selection" value={planSerCate} onChange={event=>{setPlanSerCate(event.currentTarget.value);}}>
            {SerArr.map((obj,idx) => <option key={idx} value={obj.name}>{obj.name}</option>)}          
            </select>
            </label>
       </div>
       <div>
            <label>Qty<br></br>
            <input type="number" onBlur={event=>{setQty(parseInt(event.target.value))}}/>
            </label>
       </div>
       
        <div>
        <button onClick={save} type="button" className="btn btn-info btn-md">Save</button>
        <button onClick={event=>setPlanSerCateModal(!planSerCateModal)} type="button" className="btn btn-info btn-md">Cancel</button>      
        </div>    
    </div>
    </>
    )
}
export default TbPlanSerCate
