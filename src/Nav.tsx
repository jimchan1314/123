import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { push } from 'connected-react-router';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';


function Nav() {
    const [showNavListItem, setShowNavListItem] = useState(false)
    const dispatch = useDispatch()  


    return (
        <>
        <div className="container-fluid">
        <div className="row">
                <div className="col-lg-2"></div>
                <div className="col-lg-5 col-md-10 col-sm-12">
                <div className="flex-div-end">
                <button onClick={event=>dispatch(push('/searchOrder'))} type="button" className="btn btn-outline-secondary btn-md">Order</button>
                <button onClick={event=>dispatch(push('/order'))} type="button" className="btn btn-outline-secondary btn-md">Add Order</button>
                <button onClick={event=>dispatch(push('/ticket'))} type="button" className="btn btn-outline-secondary btn-md">Ticket</button>
                <button type="button" className="btn btn-outline-secondary btn-md">Add Ticket</button>
                </div>
                </div>
                
                <div className="col-lg-5 col-md-10 col-sm-12">
                
                <div className="flex-div-end">
                <h3>Menu</h3>         
                <button className="icon_btn" onClick={event=>setShowNavListItem(!showNavListItem)}><i className="fas fa-sort-down icon"></i></button>
                </div>
                
                
                <div className="flex-div-end">
                <ul className="list-group" data-show={showNavListItem}>
                <button className="icon_btn" onClick={event=>setShowNavListItem(!showNavListItem)}><i className="fas fa-sort-up icon"></i></button>
                <div className="row">
                <li onClick={event=>{dispatch(push('/searchOrder'));setShowNavListItem(!showNavListItem)}} className="list-group-item list-group-outline-secondary">Order</li>
                <li onClick={event=>{dispatch(push('/order'));setShowNavListItem(!showNavListItem)}} className="list-group-item list-group-outline-secondary">Add Order</li>
                </div>    
                <div className="row">
                <li className="list-group-item list-group-outline-secondary">Ticket</li>
                <li className="list-group-item list-group-outline-secondary">Add Ticket</li>
                </div>
                
                </ul>
                
                </div>
                
                
                
                </div>     
        
        </div>
        </div>
        

        </>
    )
}
export default Nav




  