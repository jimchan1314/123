import React, { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { RootState } from "./store"
import { addFseSerCateRow, delFseSerCateRow } from "./fseSerCateRows/action"

function TbFseSerCate() {
    const [showFseSerCateModal, setShowFseSerCateModal] = useState(false)
    const [fseSerCate, setFseSerCate] = useState("")
    const SerArr = [{name:""},{name:"SerPlanA"},{name:"SerPlanB"},{name:"SerPlanC"}]
    const [qty, setQty] = useState(0)
    const dispatch = useDispatch()
  
    const fseSerCate_rows = useSelector((state: RootState) => state.fseSerCate)
    // console.log(ticket_rows)  
    const fseSerCateTbObj = {}
    // const fseSerCateTbRows = [{fseSerCate:"A",qty:2},{fseSerCate:"B",qty:3},{fseSerCate:"C",qty:3}]
    function save(){
        Object.assign(fseSerCateTbObj,{fseSerCate:fseSerCate})
        Object.assign(fseSerCateTbObj,{qty:qty})
        
        // console.log(purTicketTbObj)
        dispatch(addFseSerCateRow(fseSerCateTbObj))
        setShowFseSerCateModal(!showFseSerCateModal)
    }
    return (
    <>
    <div className="fseSerCateModal" data-show={showFseSerCateModal}>
       <div>
            <label>FSE Service Category<br></br>
            <select className="selection" value={fseSerCate} onChange={event=>{setFseSerCate(event.currentTarget.value);}}>
            {SerArr.map((obj,idx) => <option key={idx} value={obj.name}>{obj.name}</option>)}          
            </select>
            </label>
       </div>
       <div>
            <label>Qty<br></br>
            <input type="number" onBlur={event=>{setQty(parseInt(event.target.value))}}/>
            </label>
       </div>
       
        <div>
        <button onClick={save} type="button" className="btn btn-info btn-md">Save</button>
        <button onClick={event=>setShowFseSerCateModal(!showFseSerCateModal)} type="button" className="btn btn-info btn-md">Cancel</button>      
        </div>    
    </div>

    <div className="Ma">
            <table className="fseSerTb">
            <tbody>
              <tr>
              <th className="col_1">
              FSE Service Cate
              </th>
              <th className="col_2">
              Qty
              </th>
              <th className="col_3">
              <button className="icon_btn" onClick={event=>setShowFseSerCateModal(!showFseSerCateModal)}><i className="fas fa-search"></i></button>
              </th>
              </tr>
              
              
              {fseSerCate_rows.fseSerCate.length>0 && fseSerCate_rows.fseSerCate.map((obj,idx) => {
                let lists = []
                lists.push(
                <tr key={idx}>
                <td className="col_1">
                {obj.fseSerCate}
                </td>
                <td className="col_2">
                {obj.qty}
                </td>
                <td className="col_3">
                <button className="icon_btn" onClick={event=>dispatch(delFseSerCateRow(idx))}><i className="fas fa-times"></i></button>
                </td>
                
                </tr>
                )
                return lists
              })}
              
              
            </tbody>
            </table>
    </div>
    
    </>
    )
}
export default TbFseSerCate
