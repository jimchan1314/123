import { PlanSerCateActions } from "./action";
// reducer is to store old and new status of memo
export interface PlanSerCate {
  planSerCate: string;
  qty: number;
}

export interface PlanSerCateState {
  planSerCate: PlanSerCate[]
}

const initialState: PlanSerCateState = {
  planSerCate: [
    
  ]
};

// because action.ts create action name, we can switch relative method depends on action name
export const PlanSerCateReducer = (oldState: PlanSerCateState = initialState, action: PlanSerCateActions): PlanSerCateState => {
  switch (action.type) {
    case "@@PLAN_SER_CATE/LOADED_ROW":
      // return the ...oldState and memos: action.memos new state 
      return {
        ...oldState, 
        planSerCate: action.planSerCate_rows
      }
    case "@@PLAN_SER_CATE/ADD_ROW":
      const oldArr = oldState.planSerCate.slice();
      const newArr = action.arr as PlanSerCate[] 
      const newRows = oldArr.concat(newArr)
      // newMemos[action.i].content = action.content
      return {
        ...oldState,
        planSerCate: newRows
      }
    case "@@PLAN_SER_CATE/DEL_ROW":
      const oldArr2 = oldState.planSerCate.slice();
      oldArr2.splice(action.idx,1)
      return {
        ...oldState,
        planSerCate: oldArr2
      }
    case "@@PLAN_SER_CATE/DEL_TB":
      const oldArr1 = oldState.planSerCate.slice();
      oldArr1.splice(0)
      return {
        ...oldState,
        planSerCate: oldArr1
      }
  }
  
  return oldState;
}
