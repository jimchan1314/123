import { PlanSerCate } from "./reducer";
// action is to create action name for reducer to call the relative method
export function loadPlanSerCateRow(planSerCate_rows: PlanSerCate[]) {
  return {
    type: "@@PLAN_SER_CATE/LOADED_ROW" as  "@@PLAN_SER_CATE/LOADED_ROW",
    planSerCate_rows
  };
}

export function addPlanSerCateRow(obj: Object) {
  let arr = []
  arr.push(obj)
  // console.log(arr)
  // console.log(obj)
  return {
    type: "@@PLAN_SER_CATE/ADD_ROW" as  "@@PLAN_SER_CATE/ADD_ROW",
    arr
  };
}
export function delPlanSerCateRow(idx: number) {
  
  // console.log(idx)
  // console.log(obj)
  return {
    type: "@@PLAN_SER_CATE/DEL_ROW" as  "@@PLAN_SER_CATE/DEL_ROW",
    idx
  };
}
export function delPlanSerCateTb() {
  
  // console.log(idx)
  // console.log(obj)
  return {
    type: "@@PLAN_SER_CATE/DEL_TB" as "@@PLAN_SER_CATE/DEL_TB",
    
  };
}


export type PlanSerCateActions = ReturnType<typeof loadPlanSerCateRow> | ReturnType<typeof addPlanSerCateRow> | 
ReturnType<typeof delPlanSerCateRow> | ReturnType<typeof delPlanSerCateTb>
